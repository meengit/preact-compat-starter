import Start from '../index.jsx';
import render from 'preact-render-to-string';
import { shallow } from 'enzyme';
import React from 'react';
// import utils from 'preact-test-utils';

describe('Test <Start /> ', () => {
  const v = '1.00';
  const wrapper = shallow(<Start className="test" value={v} />);

  test('has valid return', () => {
    expect(wrapper.exists()).toBe(true);
  });

  test('has valid DOM structure', () => {
    expect(wrapper.find('div').props().className).toEqual('test');
    expect(wrapper.find('div').text()).toEqual(v);
  });

  test('tree to match snapshot', () => {
    const tree = render(Start);
    expect(tree).toMatchSnapshot();
  });
});
