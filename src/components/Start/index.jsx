import React from 'react';

const Start = ({ ...props }) => {
  const { className, value } = props;
  return <div class={className}>{value}</div>;
};

Start.propTypes = {
  value: React.PropTypes.string.isRequired
};

export default Start;
