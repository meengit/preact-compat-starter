import React from 'react';
import Start from './components/Start';

class App extends React.Component {
  render() {
    return (
      <div>
        <h1>Hello React Starter</h1>
        <Start className="first" value="I am the start component" />
      </div>
    );
  }
}

React.render(<App />, document.getElementById('preact-root'));
