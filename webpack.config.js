require('@babel/register');

const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const PORT = process.env.PORT;
const ENV = process.env.NODE_ENV;

const config = {
  mode: ENV,
  entry: {
    starter: ['./src/index.jsx']
  },
  target: 'web',
  output: {
    path: path.join(__dirname, '/dist'),
    filename: '[name].js'
  },
  module: {
    rules: [
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        use: ['babel-loader']
      }
      // If needed:
      // npm i css-loader style-loader
      // then remove comments and add your css
      //
      // {
      //   test: /\.css$/,
      //   use: ['style-loader', 'css-loader']
      // }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: ENV !== 'production',
      title: 'Starter DEV HTML',
      template: path.join(__dirname, '/src/static/index.html')
    })
  ],
  resolve: {
    modules: [path.resolve('./src'), path.resolve('./node_modules')],
    extensions: ['.js', '.jsx'],
    alias: {
      react: 'preact-compat',
      'react-dom': 'preact-compat',
      'create-react-class': 'preact-compat/lib/create-react-class',
      'react-dom-factories': 'preact-compat/lib/react-dom-factories'
    }
  },
  devServer: {
    contentBase: path.join(__dirname, '/dist'),
    compress: true,
    port: PORT || 3000,
    open: true,
    stats: {
      assets: false,
      children: false,
      chunks: false,
      chunkModules: false,
      colors: true,
      entrypoints: false,
      hash: false,
      modules: false,
      timings: false,
      version: false
    },
    watchOptions: {
      aggregateTimeout: 300,
      poll: 1000
    }
  },
  watch: false,
  devtool: ENV === 'production' ? 'none' : 'source-map',
  optimization: {
    minimize: ENV === 'production'
  }
};

module.exports = config;
