module.exports = {
  testMatch: ['src/**/__tests__/**/*.js?(x)', '**/?(*.)+(spec|test).js?(x)'],
  setupFilesAfterEnv: ['<rootDir>/setup.tests.js'],
  snapshotSerializers: ['enzyme-to-json/serializer']
};
