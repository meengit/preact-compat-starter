module.exports = function(wallaby) {
  return {
    debug: true,
    files: [
      'setup.tests.js',
      'src/components/**/*.jsx',
      '!src/components/**/__tests__/*test.jsx'
    ],
    tests: ['src/components/**/__tests__/*test.jsx'],
    env: {
      type: 'node',
      runner: 'node'
    },
    compilers: {
      '**/*.js?(x)': wallaby.compilers.babel()
    },
    testFramework: 'jest'
  };
};
