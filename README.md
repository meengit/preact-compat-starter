# Simple Preact/Compat Starter

Simple Preact/Compat Starter-kit.

## Prerequisites

- Based on Preact with Preact Compat
- Bundler: Webpack
- Linter: ESLint & Prettier
- Tests: Jest & Enzyme

## Installation

Set Node Version (Mac only if [NVM](https://github.com/nvm-sh/nvm) is available):

```bash
$ nvm use
```

Install dependencies:

```bash
npm ci
```

## npm Scripts

```json
"scripts": {
    "watch": "webpack --watch",
    "develop": "cross-env PORT=8080 NODE_ENV=development webpack-dev-server --config webpack.config.js",
    "build": "cross-env NODE_ENV=production webpack --config webpack.config.js",
    "test": "jest --config jest.config.js",
    "test-update": "npm test -- -u"
  }
```
